variable "ami1" {
  default = "ami-0aeeebd8d2ab47354"
}

variable "ins-type" {
  default = "t2.micro"
}

variable "keyname" {
  default = "aws-instance-tf"
}

variable "devicename" {
  default = "/dev/sdh"
}

variable "availablity-zone" {
	default = "us-east-1a"
}

variable "availablity-zone1" {
  default = "us-east-1b"
}

variable "vpc-cidr" {
  default = "10.0.0.0/16"
}

variable "subnets_cidr" {
	default = "10.0.1.0/24"
}

variable "subnets_cidr1" {
  default = "10.0.3.0/24"
}

variable "subnet-ids" {
  type = list
  default = ["10.0.1.0/24", "10.0.2.0/24"]
  }

  variable "availability-zoness" {
  type = string
  default = "us-east-1a"
}

variable "" {
  default = ""
}
/*
variable "subnet_ids" {
  type = list
}
*/
