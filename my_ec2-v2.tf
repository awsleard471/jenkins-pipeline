terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.42.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
  access_key = "AKIA6L6MRKPTJXVVJMK3"
  secret_key = "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"
}

#VPC Create
resource "aws_vpc" "my-fst-vpc-tfm" {
  cidr_block = "${var.vpc-cidr}"

  tags = {
    Name = "my-first-terraform-vPC"
  }
}

#Create Subnet
resource "aws_subnet" "aws-subnet1" {
  vpc_id = "${aws_vpc.my-fst-vpc-tfm.id}"
  #count = "${var.subnet-ids["count"]}"
  cidr_block = "10.0.1.0/24"
 #cidr_block = "${var.subnets_cidr}"
 # cidr_block = ["${element(split(",", var.subnet-ids ["subnet1"]), 0)}"]
  availability_zone = "us-east-1a"
 #availability_zone = ["${element(split(",", var.availability-zones ["az1"]), count.index)}"]
  #cidr_block = ["${var.subnets_cidr.id}"]
  #availability_zone = "${element(var.availablity-zone,count.index)}"
#  availability_zone = azs

#  azs = abc
#  a = ap-south-1c
#  b = ap-south-1d
#  c = ap-south-1a

  map_public_ip_on_launch = true

  tags = {
    Name = "aws-subnet1"
  }
}

resource "aws_subnet" "aws-subnet2" {
  vpc_id = "${aws_vpc.my-fst-vpc-tfm.id}"
  #cidr_block = "${var.subnets_cidr1}"
  cidr_block = "10.0.2.0/24"
  availability_zone = "us-east-1b"
  map_public_ip_on_launch = true

  tags = {
    Name = "aws-subnet2"
    }
}



#Create Internet Gateway and Attach to VPC
resource "aws_internet_gateway" "my-fst-igw-tfm" {
  vpc_id = "${aws_vpc.my-fst-vpc-tfm.id}"

  tags = {
    Name = "first-internet-gateway"
  }
}


#Route table

resource "aws_route_table" "aws-subnet1-rt" {
  vpc_id = "${aws_vpc.my-fst-vpc-tfm.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.my-fst-igw-tfm.id}"
  }

  tags = {
    Name = "route-table"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id = "${aws_subnet.aws-subnet1.id}"
  route_table_id = "${aws_route_table.aws-subnet1-rt.id}"
}



#Security Group
resource "aws_security_group" "apache-web-tf" {
  name = "allow_http"
  description = "Allow http inbound traffic for Apache"
  vpc_id = "${aws_vpc.my-fst-vpc-tfm.id}"

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


/*
  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

*/


  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#IAM Role
resource "aws_iam_instance_profile" "my-aiip1" {
  name = "my-aiip1"
  role = aws_iam_role.role.name
}

resource "aws_iam_role_policy_attachment" "s3-attach" {
  role = aws_iam_role.role.name
  policy_arn = aws_iam_policy.my-iam-policy.arn
}

resource "aws_iam_role" "role" {
  name = "my-iam-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "my-iam-policy" {
  name        = "iam-policy1"
  description = "first policy through terraform"

  policy = <<EOF
{
  "Id": "Policy1625694655363",
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Stmt1625694643884",
      "Action": [
        "s3:CreateJob",
        "s3:DeleteObject",
        "s3:GetBucketAcl",
        "s3:GetObject",
        "s3:ListBucket",
        "s3:ListJobs",
        "s3:PutObject",
        "s3:PutObjectAcl"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:s3:::test-bucket-471/apache.sh"
    }
  ]

}
EOF
}


#Volume
resource "aws_ebs_volume" "volume1" {
  availability_zone = aws_instance.my_ec2.availability_zone
  #availability_zone = aws_autoscaling_group.example.availability_zone
  size = "1"
  type = "gp2"

}

resource "aws_volume_attachment" "ebs_att" {
  device_name = "/dev/sdh"
  volume_id = aws_ebs_volume.volume1.id
  instance_id = aws_instance.my_ec2.id
  #instance_id = aws_autoscaling_group.example.id

}

data "template_file" "initial" {
  template = "${file("user-data.sh")}"
}

#Create Instance
resource "aws_instance" "my_ec2" {
  ami = "ami-0aeeebd8d2ab47354"
  instance_type = "t2.micro"
  iam_instance_profile = "my-aiip1"
  key_name = "aws-instance-tf"
  security_groups = ["${aws_security_group.apache-web-tf.id}"]
  subnet_id = "${aws_subnet.aws-subnet1.id}"
  #vpc_security_group_ids = ["sg-05f08394f904142d1"]
  user_data = data.template_file.initial.rendered

  tags = {
    Name = "apache-1"
  }
}

data "aws_subnet_ids" "subnet" {
  vpc_id = "${aws_vpc.my-fst-vpc-tfm.id}"
}
#Load Balancer Target Group - https://docs.aws.amazon.com/elasticloadbalancing/latest/APIReference/API_CreateTargetGroup.html
resource "aws_lb_target_group" "my-first-aws-tg" {
  name = "my-first-tg"
  port = 80
  protocol = "HTTP"
  target_type = "instance"
  vpc_id = aws_vpc.my-fst-vpc-tfm.id

  health_check {
    interval = 30
    path = "/"
    protocol = "HTTP"
    timeout = 5
    healthy_threshold = 5
    unhealthy_threshold = 2
  }
}

#Load Balancer
resource "aws_lb" "first-lb-tfm" {
  name = "lb-tf"
  internal = false
  load_balancer_type = "application"
  security_groups = ["${aws_security_group.apache-web-tf.id}"]
  subnets = ["${aws_subnet.aws-subnet1.id}","${aws_subnet.aws-subnet2.id}"]
  #subnets = ["${aws_subnet.aws_subnet2.id}"]
  #subnets = ["${split(",", subnet-ids)}"]
  ip_address_type = "ipv4"

  tags = {
    Name = "First Application Load Balancer via Terraform"
  }
}

resource "aws_alb_listener" "first-alb-listener-tfm" {
  load_balancer_arn = aws_lb.first-lb-tfm.arn
  port = 80
  protocol = "HTTP"

  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.my-first-aws-tg.arn
  }
}

resource "aws_lb_target_group_attachment" "attach_ec2" {
  target_group_arn = aws_lb_target_group.my-first-aws-tg.arn
  target_id = aws_instance.my_ec2.id
  #availability_zone = [${element(split(",", var.availability-zoness), 0)}]
  #availability_zone = ["us-east-1a", "us-east-1b"]
  #availability_zone = ["${var.availability-zoness}"]
  #availability_zones = ["${element(split(",", var.availability-zones ["az1"]), [count.index])}"]
  port = 80
}
